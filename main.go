package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Student struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	StudentName string             `json:"studentname" bson:"studentname"`
	Schoolno    string             `json:"schoolno" bson:"schoolno"`
	Section     string             `json:"section" bson:"section"`
}

func ConnectDB() *mongo.Collection {

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database("Student").Collection("Info")

	return collection
}
func poststudent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	var student Student
	_ = json.NewDecoder(r.Body).Decode(&student)
	collection := ConnectDB()
	result, err := collection.InsertOne(context.TODO(), student)
	if err != nil {
		log.Fatal(err)
		return
	}

	json.NewEncoder(w).Encode(result)

}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/student", poststudent).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", r))
}
