import './App.css';
import React from 'react';
import Header from './component/header';
import ReactDOM from 'react-dom';
import AddInput from './component/addInput';

class App extends React.Component{


  render(){
    return(
      <div className='App'>
        <div className='student-page'>
          <Header/>
          <AddInput />
        </div>
      </div>
    );
  }
}

export default App;
