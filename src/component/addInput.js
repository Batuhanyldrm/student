import React from 'react';
import './addInput.css';
import axios from 'axios';

class AddInput extends React.Component{
    state={
        studentname:"",
        schoolno:"",
        section:""
    }

    handlename=(e)=>{
        this.setState({studentname: e.target.value});
    }
    handleno=(e)=>{
        this.setState({schoolno: e.target.value});
    }

    handlesection=(e) => {
        this.setState({section: e.target.value});
    }

    handlestd=(event)=>{
        event.preventDefault();
 
 
        axios.post("http://localhost:8080/api/student", { 
 
        studentname:this.state.studentname,
        schoolno:this.state.schoolno,
        section:this.state.section },{
         headers: {
           "Content-Type": "application/x-www-form-urlencoded",
         },
       })
        .then(res => {
          console.log(res);
          console.log(res.data);
 
        })
 
    }

    

    render(){
        return(
            
            <div>
                <form onSubmit={this.handlestd}>
                <input type='text' placeholder='Student Name' onChange={this.handlename} />
                <input type='text' placeholder='School No' onChange={this.handleno} />
                <input type='text' placeholder='Section'  onChange={this.handlesection} />
                <button className='btn btn-primary' type='submit'>Submit</button>
                </form> 
            </div>
        )
    }
}
export default AddInput;